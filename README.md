# README #

This is a custom module for CMSMS.

### What is this repository for? ###

* This module was developed for a custom project with CMSMS.
* Version 1.0

### How do I get set up? ###

* Clone this module into the modules directory on your CMSMS instance.