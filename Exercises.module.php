<?php
if( !defined('CMS_VERSION') ) exit;

class Exercises extends CMSModule {

    const MANAGE_MODULE = 'Manage Exercises';

    const DB_MAIN_TABLE_NAME = 'cms_module_exercises';

    const ES_DETAIL_PAGE_ID = '';
    const EN_DETAIL_PAGE_ID = '';
    const PT_DETAIL_PAGE_ID = '';

    public function GetVersion() { 
        return '1.0'; 
    }

    public function GetFriendlyName() { 
        return $this->Lang('friendlyname'); 
    }

    public function GetAdminDescription() { 
        return $this->Lang('admindescription'); 
    }

    public function IsPluginModule() { 
        return true; 
    }

    public function HasAdmin() { 
        return true; 
    }

    function GetAdminSection() { 
        return 'content'; 
    }

    public function VisibleToAdminUser() { 
        return true; 
    }

    public function GetAuthor() { 
        return 'Omar Alegre'; 
    }

    public function GetAuthorEmail() { 
        return 'omar@p3design.com'; 
    }

    public function InitializeFrontend() {
        $this->SetParameterType('slug',CLEAN_STRING);
        $this->SetParameterType('lang',CLEAN_STRING);

        if(self::ES_DETAIL_PAGE_ID != '') {
            $this->RegisterRoute('/(?P<lang>.*?)\/ejercicio\/(?P<slug>.*?)$/', array('action' => 'detail', 'returnid' => self::ES_DETAIL_PAGE_ID));
        }
        if(self::EN_DETAIL_PAGE_ID != '') {
            $this->RegisterRoute('/(?P<lang>.*?)\/exercise\/(?P<slug>.*?)$/', array('action' => 'detail', 'returnid' => self::EN_DETAIL_PAGE_ID));
        }
        if(self::PT_DETAIL_PAGE_ID != '') {
            $this->RegisterRoute('/(?P<lang>.*?)\/exercicio\/(?P<slug>.*?)$/', array('action' => 'detail', 'returnid' => self::PT_DETAIL_PAGE_ID));
        }
    }

    public function get_pretty_url($id, $action, $returnid = '', $params = array(), $inline = false) {
        if( $action != 'detail' || !isset($params['slug'])) return '';

        if(isset($params['lang']) && $params['lang'] == 'es') {
            return "es/ejercicio/{$params['slug']}";
        } elseif (isset($params['lang']) && $params['lang'] == 'en') {
            return "en/exercise/{$params['slug']}";
        } elseif (isset($params['lang']) && $params['lang'] == 'pt') {
            return "pt/exercicio/{$params['slug']}";
        }
    }
    
    public function SearchResultWithParams($returnid, $item_id, $attr = '', $params = '')
    {
        if( $attr != 'Exercises' ) return;

        $item = Exercise::load_by_id((int)$item_id);

        if( !$item ) return;

        $result = array();
        $result[0] = $this->GetFriendlyName();
        $result[1] = array(
            'es' => $item->es_title,
            'en' => $item->en_title
        );
        $detailpage = $returnid;
        if( isset($params['detailpage']) ) {
            $hm = CmsApp::get_instance()->GetHierarchyManager();
            $node = $hm->sureGetNodeByAlias($params['detailpage']);
            if( is_object($node) ) $detailpage = $node->get_tag('id');
        }
        $result[2] = array(
            'es' => $this->create_url('cntnt01','detail', $detailpage, array('slug' => $item->es_slug)),
            'en' => $this->create_url('cntnt01','detail', $detailpage, array('slug' => $item->en_slug))
        );
        return $result;
    }
}