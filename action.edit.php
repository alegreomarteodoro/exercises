<?php
if( !defined('CMS_VERSION') ) exit;

if( !$this->CheckPermission(Exercises::MANAGE_MODULE) ) {
    echo $this->Lang('access_denied');
    exit;
}

$item = new Exercise();

if( isset($params['row_id']) && $params['row_id'] > 0) {
    $item = Exercise::load_by_id((int)$params['row_id']);
}

if( isset($params['change_status']) && $params['change_status'] == 'yes' ) {
    $item->published = !$item->published;

    if($item->save()) {
        $this->SetMessage($this->Lang('item_saved'));
        $this->RedirectToAdminTab();
    }
}

if( isset($params['cancel']) ) {

    $this->RedirectToAdminTab();

} elseif( isset($params['submit']) ) {

    $item->es_title = trim($params['es_title']);
    $item->es_youtube_video_url = trim($params['es_youtube_video_url']);
    $item->es_starting_position_image = $params['es_starting_position_image'];
    $item->es_final_position_image = $params['es_final_position_image'];
    $item->es_tips = trim($params['es_tips']);
    $item->es_picture_of_required_item = $params['es_picture_of_required_item'];

    $item->en_title = trim($params['en_title']);
    $item->en_youtube_video_url = trim($params['en_youtube_video_url']);
    $item->en_starting_position_image = $params['en_starting_position_image'];
    $item->en_final_position_image = $params['en_final_position_image'];
    $item->en_tips = trim($params['en_tips']);
    $item->en_picture_of_required_item = $params['en_picture_of_required_item'];

    $item->pt_title = trim($params['pt_title']);
    $item->pt_youtube_video_url = ($params['pt_youtube_video_url']);
    $item->pt_starting_position_image = $params['pt_starting_position_image'];
    $item->pt_final_position_image = $params['pt_final_position_image'];
    $item->pt_tips = trim($params['pt_tips']);
    $item->pt_picture_of_required_item = $params['pt_picture_of_required_item'];
    
    $item->published = cms_to_bool($params['published']);

    if($item->save()) {

        $this->SetMessage($this->Lang('item_saved'));
        $this->RedirectToAdminTab();

    } else {

        $this->ShowErrors($this->Lang('item_error'));

    }
}

$tpl = $smarty->CreateTemplate($this->GetTemplateResource('admin/edit.tpl'),null,null, $smarty);
$tpl->assign('item', $item);
$tpl->display();