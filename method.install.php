<?php
if( !defined('CMS_VERSION') ) exit;

$this->CreatePermission(Exercises::MANAGE_MODULE);

$db = $this->GetDb();
$dict = NewDataDictionary($db);
$taboptarray = array('mysql' => 'TYPE=MyISAM');
$flds = "
 id I KEY AUTO,
 es_slug C(255) NULL,
 es_title C(255) NULL,
 es_youtube_video_url C(255) NULL,
 es_starting_position_image C(255) NULL,
 es_final_position_image C(255) NULL,
 es_tips C(255) NULL,
 es_picture_of_required_item C(255) NULL,
 en_slug C(255) NULL,
 en_title C(255) NULL,
 en_youtube_video_url C(255) NULL,
 en_starting_position_image C(255) NULL,
 en_final_position_image C(255) NULL,
 en_tips C(255) NULL,
 en_picture_of_required_item C(255) NULL,
 pt_slug C(255) NULL,
 pt_title C(255) NULL,
 pt_youtube_video_url C(255) NULL,
 pt_starting_position_image C(255) NULL,
 pt_final_position_image C(255) NULL,
 pt_tips C(255) NULL,
 pt_picture_of_required_item C(255) NULL,
 published I1 NULL,
 created_at " . CMS_ADODB_DT . ",
 modified_at " . CMS_ADODB_DT . "
";
$sqlarray = $dict->CreateTableSQL(Exercises::DB_MAIN_TABLE_NAME, $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);