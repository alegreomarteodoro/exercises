<?php
class Exercise
{
    public $id = 0;
    public $created_at = '';
    public $modified_at = '';
    public $published = false;

    public $es_slug = '';
    public $es_title = '';
    public $es_youtube_video_url = '';
    public $es_starting_position_image = '';
    public $es_final_position_image = '';
    public $es_tips = '';
    public $es_picture_of_required_item = '';
    
    public $en_slug = '';
    public $en_title = '';
    public $en_youtube_video_url = '';
    public $en_starting_position_image = '';
    public $en_final_position_image = '';
    public $en_tips = '';
    public $en_picture_of_required_item = '';

    public $pt_slug = '';
    public $pt_title = '';
    public $pt_youtube_video_url = '';
    public $pt_starting_position_image = '';
    public $pt_final_position_image = '';
    public $pt_tips = '';
    public $pt_picture_of_required_item = '';

    public function save()
    {
        if( !$this->is_valid() ) return false;
        if( $this->id > 0 ) {
            return $this->update();
        } else {
            return $this->insert();
        }
        return false;
    }

    public function is_valid()
    {
        return true;
    }

    protected function insert()
    {
        $db = \cms_utils::get_db();
        $sql = 'INSERT INTO ' . Exercises::DB_MAIN_TABLE_NAME . ' (es_slug, es_title, es_youtube_video_url, es_starting_position_image, es_final_position_image, es_tips, es_picture_of_required_item, en_slug, en_title, en_youtube_video_url, en_starting_position_image, en_final_position_image, en_tips, en_picture_of_required_item, pt_slug, pt_title, pt_youtube_video_url, pt_starting_position_image, pt_final_position_image, pt_tips, pt_picture_of_required_item, published, created_at, modified_at) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
        $dbr = $db->Execute($sql,array(
            $this->es_slug,
            $this->es_title,
            $this->es_youtube_video_url,
            $this->es_starting_position_image,
            $this->es_final_position_image,
            $this->es_tips,
            $this->es_picture_of_required_item,
            $this->en_slug,
            $this->en_title,
            $this->en_youtube_video_url,
            $this->en_starting_position_image,
            $this->en_final_position_image,
            $this->en_tips,
            $this->en_picture_of_required_item,
            $this->pt_slug,
            $this->pt_title,
            $this->pt_youtube_video_url,
            $this->pt_starting_position_image,
            $this->pt_final_position_image,
            $this->pt_tips,
            $this->pt_picture_of_required_item,
            $this->published,
            date("Y-m-d H:i:s"),
            date("Y-m-d H:i:s"),
        ));
        if( !$dbr ) return false;
        $this->id = $db->Insert_ID();
        return true;
    }

    protected function update()
    {
        $db = \cms_utils::get_db();
        $sql = 'UPDATE ' . Exercises::DB_MAIN_TABLE_NAME . ' SET es_slug = ?, es_title = ?, es_youtube_video_url = ?, es_starting_position_image = ?, es_final_position_image = ?, es_tips = ?, es_picture_of_required_item = ?, en_slug = ?, en_title = ?, en_youtube_video_url = ?, en_starting_position_image = ?, en_final_position_image = ?, en_tips = ?, en_picture_of_required_item = ?, pt_slug = ?, pt_title = ?, pt_youtube_video_url = ?, pt_starting_position_image = ?, pt_final_position_image = ?, pt_tips = ?, pt_picture_of_required_item = ?, published = ?, created_at = ?, modified_at = ? WHERE id = ?';
        $dbr = $db->Execute($sql,array(
            $this->es_slug,
            $this->es_title,
            $this->es_youtube_video_url,
            $this->es_starting_position_image,
            $this->es_final_position_image,
            $this->es_tips,
            $this->es_picture_of_required_item,
            $this->en_slug,
            $this->en_title,
            $this->en_youtube_video_url,
            $this->en_starting_position_image,
            $this->en_final_position_image,
            $this->en_tips,
            $this->en_picture_of_required_item,
            $this->pt_slug,
            $this->pt_title,
            $this->pt_youtube_video_url,
            $this->pt_starting_position_image,
            $this->pt_final_position_image,
            $this->pt_tips,
            $this->pt_picture_of_required_item,
            $this->published,
            $this->created_at,
            date("Y-m-d H:i:s"),
            $this->id
        ));
        if( !$dbr ) return false;
        return true;
    }

    public function delete()
    {
        if( !$this->id ) return false;
        $db = \cms_utils::get_db();
        $sql = 'DELETE FROM ' . Exercises::DB_MAIN_TABLE_NAME  . ' WHERE id = ?';
        $dbr = $db->Execute($sql,array($this->id));
        if( !$dbr ) return false;
        $this->id = 0;
        return true;
    }

    public function fill_from_array($row)
    {
        $this->id = $row['id'];
        $this->es_slug = $row['es_slug'];
        $this->es_title = $row['es_title'];
        $this->es_youtube_video_url = $row['es_youtube_video_url'];
        $this->es_starting_position_image = $row['es_starting_position_image'];
        $this->es_final_position_image = $row['es_final_position_image'];
        $this->es_tips = $row['es_tips'];
        $this->es_picture_of_required_item = $row['es_picture_of_required_item'];

        $this->en_slug = $row['en_slug'];
        $this->en_title = $row['en_title'];
        $this->en_youtube_video_url = $row['en_youtube_video_url'];
        $this->en_starting_position_image = $row['en_starting_position_image'];
        $this->en_final_position_image = $row['en_final_position_image'];
        $this->en_tips = $row['en_tips'];
        $this->en_picture_of_required_item = $row['en_picture_of_required_item'];

        $this->pt_slug = $row['pt_slug'];
        $this->pt_title = $row['pt_title'];
        $this->pt_youtube_video_url = $row['pt_youtube_video_url'];
        $this->pt_starting_position_image = $row['pt_starting_position_image'];
        $this->pt_final_position_image = $row['pt_final_position_image'];
        $this->pt_tips = $row['pt_tips'];
        $this->pt_picture_of_required_item = $row['pt_picture_of_required_item'];

        $this->published = $row['published'];
        $this->created_at = $row['created_at'];
        $this->modified_at = $row['modified_at'];

        return $this;
    }

    public static function &load_by_id($id)
    {
        $id = (int) $id;
        $db = \cms_utils::get_db();
        $sql = 'SELECT * FROM ' . Exercises::DB_MAIN_TABLE_NAME  . ' WHERE id = ?';
        $row = $db->GetRow($sql,array($id));
        if( is_array($row) ) {
            $obj = new self();
            $obj->fill_from_array($row);
            return $obj;
        }
    }

    public static function &load_by_slug($slug, $lang = 'es')
    {
        $db = \cms_utils::get_db();
        $sql = 'SELECT * FROM '. Exercises::DB_MAIN_TABLE_NAME  .' WHERE es_slug = ?';

        if($lang == 'en') {
            $sql = 'SELECT * FROM '. Exercises::DB_MAIN_TABLE_NAME  .' WHERE en_slug = ?';
        }

        if($lang == 'pt') {
            $sql = 'SELECT * FROM '. Exercises::DB_MAIN_TABLE_NAME  .' WHERE pt_slug = ?';
        }

        $row = $db->GetRow($sql, array($slug));

        if( is_array($row) ) {
            $obj = new self();
            $obj->fill_from_array($row);
            return $obj;
        }

        return null;
    }

    public function get_detail_url($lang = 'es')
    {
        $module_instance = cms_utils::get_module('Exercises');
        if ($lang == 'es') {
            $detailpage = Exercises::ES_DETAIL_PAGE_ID;
            return  $module_instance->create_url('cntnt01', 'detail', $detailpage, array('slug'=>$this->es_slug, 'returnid' => $detailpage, 'lang' => 'es'));
        } elseif ($lang == 'en') {
            $detailpage = Exercises::EN_DETAIL_PAGE_ID;
            return  $module_instance->create_url('cntnt01', 'detail', $detailpage, array('slug'=>$this->en_slug, 'returnid' => $detailpage, 'lang' => 'en'));
        } elseif ($lang == 'pt') {
            $detailpage = Exercises::EN_DETAIL_PAGE_ID;
            return  $module_instance->create_url('cntnt01', 'detail', $detailpage, array('slug'=>$this->pt_slug, 'returnid' => $detailpage, 'lang' => 'pt'));
        }
    }
}
