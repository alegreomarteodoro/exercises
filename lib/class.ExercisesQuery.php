<?php
class ExercisesQuery extends CmsDbQueryBase
{
    public $current_page = 1;
    public $lang = 'es';
    
    public function __construct($args = '')
    {
        parent::__construct($args);
        if( isset($this->_args['limit']) ) $this->_limit = (int) $this->_args['limit'];
        if( isset($this->_args['pagenumber']) ) $this->current_page = (int) $this->_args['current_page'];
        if( isset($this->_args['lang']) ) $this->lang = $this->_args['lang'];
        $this->_offset = ($this->current_page - 1) * $this->_limit;
    }

    public function execute()
    {
        if( !is_null($this->_rs) ) return;

        $sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM ' . Exercises::DB_MAIN_TABLE_NAME;
        $where = array();
        
        if( isset($this->_args['published']) ) {
            $published = $this->_args['published'];
            $where[] = ' published = ' . $published;
        }

        if( isset($this->_args['in']) ) {
            $in = $this->_args['in'];
            $where[] = ' id IN (' . $in . ') ';
        }

        if( isset($this->_args['search_string']) ) {
            $search_string = $this->_args['search_string'];
            $where[] = '( es_title LIKE "%' . $search_string . '%" OR en_title LIKE "%' . $search_string . '%" OR pt_title LIKE "%' . $search_string . '%" )';
        }

        if(count($where) > 0) {
            $sql .= ' WHERE ' . implode(' AND ', $where);
        }

        if( isset($this->_args['order_by']) ) {
            $order_by = $this->_args['order_by'];
            $sql .= ' ORDER BY ' . $order_by . ' ASC';
        } else {
            $sql .= ' ORDER BY created_at DESC';
        }
        $db = \cms_utils::get_db();
        $this->_rs = $db->SelectLimit($sql, $this->_limit, $this->_offset);
        IF( $db->ErrorMsg() ) throw new \CmsSQLErrorException($db->sql . ' -- ' . $db->ErrorMsg());
        $this->_totalmatchingrows = $db->GetOne('SELECT FOUND_ROWS()');
    }
    
    public function &GetObject()
    {
        $obj = new Exercise();
        $obj->fill_from_array($this->fields);
        return $obj;
    }
}
