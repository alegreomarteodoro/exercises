<?php
if( !defined('CMS_VERSION') ) exit;

if( !$this->CheckPermission(Exercises::MANAGE_MODULE) ) {
    echo $this->Lang('access_denied');
    exit;
}

$current_page = ($params['current_page'] ? $params['current_page'] : 1);

$args = array( 'limit' => 50, 'current_page' => $current_page );

if(isset($params['search_string']) && $params['search_string'] != '') {
    $args['search_string'] = $params['search_string'];
}

$query = new ExercisesQuery($args);
$items = $query->GetMatches();

$tpl = $smarty->CreateTemplate($this->GetTemplateResource('admin/defaultadmin.tpl'),null,null, $smarty);
$tpl->assign('items',$items);
$tpl->assign('current_page', $current_page);
$tpl->assign('num_of_pages', $query->numpages);
$tpl->assign('total_rows', $query->totalrows);
$tpl->assign('search_string', $params['search_string']);
$tpl->display();