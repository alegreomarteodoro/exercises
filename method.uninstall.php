<?php
if( !defined('CMS_VERSION') ) exit;

$this->RemovePermission(Exercises::MANAGE_MODULE);

$db = $this->GetDb();
$dict = NewDataDictionary($db);
$sqlarray = $dict->DropTableSQL(Exercises::DB_MAIN_TABLE_NAME);
$dict->ExecuteSQLArray($sqlarray);
