<?php
$lang['friendlyname'] = 'Exercises';
$lang['admindescription'] = 'In this module you can manage the exercises.';
$lang['access_denied'] = 'You do not have permission to access this module.';
?>