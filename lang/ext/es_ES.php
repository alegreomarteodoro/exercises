<?php
$lang['friendlyname'] = 'Ejercicios';
$lang['admindescription'] = 'En este módulo podrás administrar los ejecicios.';
$lang['access_denied'] = 'No tienes permiso para acceder a este módulo.';
$lang['search'] = 'Buscar';
$lang['add_new'] = 'Agregar nuevo';
$lang['rows'] = 'registros';
$lang['delete_item'] = 'Eliminar';
$lang['created_date'] = 'Fecha de creación';
$lang['publish'] = 'Publicar';
$lang['do_not_publish'] = 'No publicar';
$lang['save'] = 'Guardar';
$lang['cancel'] = 'Cancelar';
$lang['slug'] = 'Slug';
$lang['configuration'] = 'Configuración';
$lang['title'] = 'Nombre';
$lang['video'] = 'Video (Link a youtube)';
$lang['starting_position_image'] = 'Imagen con posición de inicio del ejercicio';
$lang['final_position_image'] = 'Imagen con posición final del ejercicio';
$lang['tips'] = 'Tips';
$lang['picture_of_required_item'] = 'Imagen del elemento necesario para hacer el ejercicio';
$lang['slug_description'] = '';
$lang['item_saved'] = 'Los datos se han guardado correctamente.';
$lang['item_error'] = 'Ha ocurrido un error mientras se guardaban los datos.';
$lang['created_at'] = 'Creado el';
$lang['modified_at'] = 'Modificado el';
$lang['item_was_deleted'] = 'El registro fue eliminado.';
?>