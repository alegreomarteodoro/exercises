<style>
    tr.row1.row-inactive,
    tr.row2.row-inactive {
        background: #FFE1E1;
    }
    tr.row1.row-inactive:hover,
    tr.row2.row-inactive:hover {
        background: #ddd
    }
</style>

<div class="pageoptions">
    <a href="{cms_action_url action=edit}">{admin_icon icon='newobject.gif'}{$mod->Lang('add_new')}</a>
</div>

<div class="pageoptions" style="display: inline-block;">
    {form_start}
        <div class="pageoverflow" style="float: left; margin-right: 20px;">
            <p class="pagetext">{$mod->Lang('search')}:</p>
            <p class="pageinput">
                <input type="text" name="{$actionid}search_string" value="{$search_string}"/>
            </p>
        </div>
        <div style="float: left; margin-right: 20px; padding-top: 32px;">
            <p class="pageinput">
                <input type="submit" name="{$actionid}submit" value="{$mod->Lang('search')}"/>
            </p>
        </div>
    {form_end}
</div>

<div class="pageoptions">
    <strong style="float: right;">({$total_rows} {$mod->Lang('rows')})</strong>
</div>

{if !empty($items)}
    <table class="pagetable">
        <thead>
        <tr>
            <th>#</th>
            <th>{$mod->Lang('title')}</th>
            <th>{$mod->Lang('created_date')}</th>
            <th>{$mod->Lang('publish')}</th>
            <th class="pageicon">{* view icon *}</th>
            <th class="pageicon">{* edit icon *}</th>
            <th class="pageicon">{* delete icon *}</th>
        </tr>
        </thead>
        <tbody>
        {foreach $items as $item}
            {cms_action_url action=edit row_id=$item->id assign='edit_url'}
            {cycle values="row1,row2" assign='rowclass'}
            <tr class="{$rowclass} {if $item->published == 0} row-inactive {/if}">
                <td>{$item->id}</td>
                <td>
                    <strong><a href="{$edit_url}" title="{$mod->Lang('edit')}">{$item->es_title|strip_tags}</a></strong>
                    {if $item->en_title}
                        <br><a href="{$edit_url}" title="{$mod->Lang('edit')}"><small>{$item->en_title|strip_tags}</small></a>
                    {/if}
                    {if $item->pt_title}
                        <br><a href="{$edit_url}" title="{$mod->Lang('edit')}"><small>{$item->pt_title|strip_tags}</small></a>
                    {/if}
                </td>
                <td>{$item->created_at|date_format:'%x'}</td>
                <td>{if $item->published == 1}
                        <a href="{cms_action_url action='edit' row_id=$item->id change_status='yes'}">
                            {admin_icon icon='true.gif' title=""}
                        </a>
                    {else}
                        <a href="{cms_action_url action='edit' row_id=$item->id change_status='yes'}">
                            {admin_icon icon='false.gif' title=""}
                        </a>
                    {/if}
                </td>
                <td><a href="{$item->get_detail_url()}" target="_blank" title="{$mod->Lang('view')}">{admin_icon icon='view.gif'}</a></td>
                <td><a href="{$edit_url}" title="{$mod->Lang('edit')}">{admin_icon icon='edit.gif'}</a></td>
                <td><a class="delete_item" href="{cms_action_url action=delete row_id=$item->id}" title="{$mod->Lang('delete')}">{admin_icon icon='delete.gif'}</a></td>
            </tr>
        {/foreach}
        </tbody>
    </table>

    {if $pagecount > 1}
        <ul style="list-style: none;">
            {cms_action_url action=defaultadmin current_page=$current_page search_string=$search_string assign='num'}

            {section name="pages" start=1 loop=$num_of_pages+1}
                <li style="display: inline-block;">
                    {if $smarty.section.pages.index == $current_page}
                        <span class="current_page pagenav">
                    {else}
                        <a class="pagenav" href="{$page_link|regex_replace:"/current_page=\d+/":"current_page=`$smarty.section.pages.index`"}">
                    {/if}
                    {$smarty.section.pages.index}
                    {if $smarty.section.pages.index == $current_page}
                        </span>
                    {else}
                        </a>
                    {/if}
                </li>
            {/section}
        </ul>
    {/if}
    
    {literal}
        <script type="text/javascript">
            $(document).ready(function(){
                $('a.delete_item').click(function(){
                    return confirm("{$mod->Lang('delete_item')}");
                })
            });
        </script>
    {/literal}

{else}
    
    <div class="information">
        <p>Aun no hay registros en <strong>{$mod->Lang('friendlyname')}</strong>.</p>
    </div>

{/if}
