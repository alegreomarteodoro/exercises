<h3>{$mod->Lang('friendlyname')}</h3>

{form_start row_id=$item->id}

    <div class="pageoverflow">
        <p class="pageinput">
            <input type="submit" name="{$actionid}submit" value="{$mod->Lang('save')}"/>
            <input type="submit" name="{$actionid}cancel" value="{$mod->Lang('cancel')}"/>
        </p>
    </div>

    <div id="page_tabs">
        <div id="es" class="active" tabindex="0">ES</div>
        <div id="en" tabindex="1">EN</div>
        <div id="pt" tabindex="2">PT</div>
        <div id="config" tabindex="3">{$mod->Lang('configuration')}</div>
    </div>

    <div class="clearb"></div>

    <div id="page_content">

        <div id="es_c" style="display: block;">

            <!--div class="pageoverflow">
                <p class="pagetext">{$mod->Lang('slug')}:</p>
                <p class="pageinput">
                    <input type="text" name="{$actionid}es_slug" value="{$item->es_slug}"/>
                    <br>
                    <small>{$mod->Lang('slug_description')}</small>
                </p>
            </div-->

            <div class="pageoverflow">
                <p class="pagetext">{$mod->Lang('title')}:</p>
                <p class="pageinput">
                    <input type="text" name="{$actionid}es_title" value="{$item->es_title}"/>
                </p>
            </div>

            <div class="pageoverflow">
                <p class="pagetext">{$mod->Lang('video')}:</p>
                <p class="pageinput">
                    <input type="text" name="{$actionid}es_youtube_video_url" value="{$item->es_youtube_video_url}"/>
                </p>
            </div>

            <div class="pageoverflow">
                <p class="pagetext">{$mod->Lang('starting_position_image')}:</p>
                <p class="pageinput">
                {if $item->es_starting_position_image}
                    {thumbnail_url file=$item->es_starting_position_image assign=tmp}
                    {if $tmp}<img src="{$tmp}" alt="{$item->es_title}"/>{/if}
                {/if}
                {cms_filepicker name="{$actionid}es_starting_position_image" value=$item->es_starting_position_image}
                </p>
            </div>

            <div class="pageoverflow">
                <p class="pagetext">{$mod->Lang('final_position_image')}:</p>
                <p class="pageinput">
                {if $item->es_final_position_image}
                    {thumbnail_url file=$item->es_final_position_image assign=tmp}
                    {if $tmp}<img src="{$tmp}" alt="{$item->es_title}"/>{/if}
                {/if}
                {cms_filepicker name="{$actionid}es_final_position_image" value=$item->es_final_position_image}
                </p>
            </div>

            <div class="pageoverflow">
                <p class="pagetext">{$mod->Lang('tips')}:</p>
                <p class="pageinput">
                    <textarea name="{$actionid}es_tips" rows="6">{$item->es_tips}</textarea>
                </p>
            </div>

            <div class="pageoverflow">
                <p class="pagetext">{$mod->Lang('picture_of_required_item')}:</p>
                <p class="pageinput">
                {if $item->es_picture_of_required_item}
                    {thumbnail_url file=$item->es_picture_of_required_item assign=tmp}
                    {if $tmp}<img src="{$tmp}" alt="{$item->es_title}"/>{/if}
                {/if}
                {cms_filepicker name="{$actionid}es_picture_of_required_item" value=$item->es_picture_of_required_item}
                </p>
            </div>

        </div>

        <div id="en_c">

            <!--div class="pageoverflow">
                <p class="pagetext">{$mod->Lang('slug')}:</p>
                <p class="pageinput">
                    <input type="text" name="{$actionid}en_slug" value="{$item->en_slug}"/>
                    <br>
                    <small>{$mod->Lang('slug_description')}</small>
                </p>
            </div-->

            <div class="pageoverflow">
                <p class="pagetext">{$mod->Lang('title')}:</p>
                <p class="pageinput">
                    <input type="text" name="{$actionid}en_title" value="{$item->en_title}"/>
                </p>
            </div>

            <div class="pageoverflow">
                <p class="pagetext">{$mod->Lang('video')}:</p>
                <p class="pageinput">
                    <input type="text" name="{$actionid}en_youtube_video_url" value="{$item->en_youtube_video_url}"/>
                </p>
            </div>

            <div class="pageoverflow">
                <p class="pagetext">{$mod->Lang('starting_position_image')}:</p>
                <p class="pageinput">
                {if $item->en_starting_position_image}
                    {thumbnail_url file=$item->en_starting_position_image assign=tmp}
                    {if $tmp}<img src="{$tmp}" alt="{$item->en_title}"/>{/if}
                {/if}
                {cms_filepicker name="{$actionid}en_starting_position_image" value=$item->en_starting_position_image}
                </p>
            </div>

            <div class="pageoverflow">
                <p class="pagetext">{$mod->Lang('final_position_image')}:</p>
                <p class="pageinput">
                {if $item->en_final_position_image}
                    {thumbnail_url file=$item->en_final_position_image assign=tmp}
                    {if $tmp}<img src="{$tmp}" alt="{$item->en_title}"/>{/if}
                {/if}
                {cms_filepicker name="{$actionid}en_final_position_image" value=$item->en_final_position_image}
                </p>
            </div>

            <div class="pageoverflow">
                <p class="pagetext">{$mod->Lang('tips')}:</p>
                <p class="pageinput">
                    <textarea name="{$actionid}en_tips" rows="6">{$item->en_tips}</textarea>
                </p>
            </div>

            <div class="pageoverflow">
                <p class="pagetext">{$mod->Lang('picture_of_required_item')}:</p>
                <p class="pageinput">
                {if $item->en_picture_of_required_item}
                    {thumbnail_url file=$item->en_picture_of_required_item assign=tmp}
                    {if $tmp}<img src="{$tmp}" alt="{$item->en_title}"/>{/if}
                {/if}
                {cms_filepicker name="{$actionid}en_picture_of_required_item" value=$item->en_picture_of_required_item}
                </p>
            </div>

        </div>

        <div id="pt_c">

            <!--div class="pageoverflow">
                <p class="pagetext">{$mod->Lang('slug')}:</p>
                <p class="pageinput">
                    <input type="text" name="{$actionid}pt_slug" value="{$item->pt_slug}"/>
                    <br>
                    <small>{$mod->Lang('slug_description')}</small>
                </p>
            </div-->

            <div class="pageoverflow">
                <p class="pagetext">{$mod->Lang('title')}:</p>
                <p class="pageinput">
                    <input type="text" name="{$actionid}pt_title" value="{$item->pt_title}"/>
                </p>
            </div>

            <div class="pageoverflow">
                <p class="pagetext">{$mod->Lang('video')}:</p>
                <p class="pageinput">
                    <input type="text" name="{$actionid}pt_youtube_video_url" value="{$item->pt_youtube_video_url}"/>
                </p>
            </div>

            <div class="pageoverflow">
                <p class="pagetext">{$mod->Lang('starting_position_image')}:</p>
                <p class="pageinput">
                {if $item->pt_starting_position_image}
                    {thumbnail_url file=$item->pt_starting_position_image assign=tmp}
                    {if $tmp}<img src="{$tmp}" alt="{$item->pt_title}"/>{/if}
                {/if}
                {cms_filepicker name="{$actionid}pt_starting_position_image" value=$item->pt_starting_position_image}
                </p>
            </div>

            <div class="pageoverflow">
                <p class="pagetext">{$mod->Lang('final_position_image')}:</p>
                <p class="pageinput">
                {if $item->pt_final_position_image}
                    {thumbnail_url file=$item->pt_final_position_image assign=tmp}
                    {if $tmp}<img src="{$tmp}" alt="{$item->pt_title}"/>{/if}
                {/if}
                {cms_filepicker name="{$actionid}pt_final_position_image" value=$item->pt_final_position_image}
                </p>
            </div>

            <div class="pageoverflow">
                <p class="pagetext">{$mod->Lang('tips')}:</p>
                <p class="pageinput">
                    <textarea name="{$actionid}pt_tips" rows="6">{$item->es_tips}</textarea>
                </p>
            </div>

            <div class="pageoverflow">
                <p class="pagetext">{$mod->Lang('picture_of_required_item')}:</p>
                <p class="pageinput">
                {if $item->pt_picture_of_required_item}
                    {thumbnail_url file=$item->pt_picture_of_required_item assign=tmp}
                    {if $tmp}<img src="{$tmp}" alt="{$item->pt_title}"/>{/if}
                {/if}
                {cms_filepicker name="{$actionid}pt_picture_of_required_item" value=$item->pt_picture_of_required_item}
                </p>
            </div>

        </div>

        <div id="config_c">

            <div class="pageoverflow">
                <p class="pagetext">{$mod->Lang('publish')}:</p>
                <p class="pageinput">
                    <select name="{$actionid}published">
                        {cms_yesno selected=$item->published}
                    </select>
                </p>
            </div>

            {if $item->created_at}
                <div class="pageoverflow">
                    <p class="pagetext">{$mod->Lang('created_at')}:</p>
                    <p class="pageinput">
                        <strong>{$item->created_at|date_format:'%x'}</strong>
                    </p>
                </div>
            {/if}

            {if $item->modified_at}
                <div class="pageoverflow">
                    <p class="pagetext">{$mod->Lang('modified_at')}:</p>
                    <p class="pageinput">
                        <strong>{$item->modified_at|date_format:'%x'}</strong>
                    </p>
                </div>
            {/if}

        </div>

    </div>

    <div class="pageoverflow">
        <p class="pageinput">
            <input type="submit" name="{$actionid}submit" value="{$mod->Lang('save')}"/>
            <input type="submit" name="{$actionid}cancel" value="{$mod->Lang('cancel')}"/>
        </p>
    </div>

{form_end}
