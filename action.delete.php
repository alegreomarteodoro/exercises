<?php
if( !defined('CMS_VERSION') ) exit;

if( !$this->CheckPermission(Exercises::MANAGE_MODULE) ) {
    echo $this->Lang('access_denied');
    exit;
}

if( isset($params['row_id']) && $params['row_id'] > 0) {
    $item = Exercise::load_by_id((int)$params['row_id']);
    $item->delete();
    $this->SetMessage($this->Lang('item_was_deleted'));
    $this->RedirectToAdminTab();
}